"""
Factories useful for testing profiles module
"""
from itertools import cycle

from django.contrib.auth import get_user_model
from factory import (
    Faker,
    Sequence,
    PostGenerationMethodCall,
    post_generation,
    SubFactory,
    lazy_attribute,
)
from factory.django import DjangoModelFactory
from oauth2_provider.models import Application

from profiles.models import Team, TeamMembership

ROLE_TAGS = [
    "ff",  # firefighter
    "sprint manager",
    "developer advocate",
]


class ApplicationFactory(DjangoModelFactory):
    """
    User factory.
    """

    client_type = "confidential"
    authorization_grant_type = "password"

    class Meta:
        model = Application


class TeamFactory(DjangoModelFactory):
    """
    Team factory.
    """

    class Meta:
        model = Team

    name = Sequence(lambda x: f"team{x}")
    description = Faker("paragraph")


class TeamWithColorFactory(DjangoModelFactory):
    """Create a Team object with color for team name"""

    class Meta:
        model = Team
        django_get_or_create = ("name",)

    name = Faker("color_name")
    description = Faker("sentence", nb_words=5)

    @classmethod
    def _adjust_kwargs(cls, **kwargs):
        """Add 'Team ' prefix to name"""
        kwargs["name"] = f"Team {kwargs['name']}"
        return kwargs


class UserFactory(DjangoModelFactory):
    """
    User factory.
    """

    username = Sequence(lambda x: f"user{x}")
    email = Sequence(lambda x: f"user{x}@example.com")
    password = PostGenerationMethodCall("set_password", "Test")

    class Meta:
        model = get_user_model()


class UserWithNameFactory(DjangoModelFactory):
    """Create User objects with fake real names"""

    class Meta:
        model = get_user_model()
        django_get_or_create = ("email",)

    first_name = Faker("first_name")
    last_name = Faker("last_name")
    password = PostGenerationMethodCall("set_password", "listaflow")

    @lazy_attribute
    def email(self):
        """Email"""
        # pylint: disable=no-member
        return f"{self.first_name.lower()}@example.com"

    @lazy_attribute
    def username(self):
        """Username"""
        return self.first_name + self.last_name


class AdminFactory(UserWithNameFactory):
    """Create a super user admin"""

    is_superuser = True
    is_staff = True
    first_name = "Admin"
    last_name = ""
    username = "admin"
    email = "admin@example.com"
    password = PostGenerationMethodCall("set_password", "admin")


class TeamMembershipFactory(DjangoModelFactory):
    """Create a team user membership"""

    class Meta:
        model = TeamMembership
        django_get_or_create = ("user", "team")

    user = SubFactory(UserWithNameFactory)
    team = SubFactory(TeamWithColorFactory)

    @post_generation
    def tags(self, create, extracted, **kwargs):
        """Provide a list of tag strings to add to this team user"""
        if not create or not extracted:
            return

        # pylint: disable=no-member
        self.tags.add(*extracted)


class TeamWithUsersFactory(TeamWithColorFactory):
    """Create a team of users"""

    @post_generation
    def members(self, create, extracted, **kwargs):
        """Provide a list of User objects to be added to Team"""
        if not create:
            return

        if not extracted:
            return

        # pylint: disable=no-member
        self.members.add(*extracted)


class TeamWithTaggedUsersFactory(TeamWithColorFactory):
    """Create a team of users with pre-assigned role tags"""

    @post_generation
    def members(self, create, extracted, **kwargs):
        """Provide a list of User objects to be added to Team"""
        if not create or not extracted:
            return

        for user, tag in zip(extracted, cycle(ROLE_TAGS)):
            TeamMembershipFactory.create(user=user, team=self, tags=[tag])
