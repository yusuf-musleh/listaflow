"""
Django admin module for workflow.
"""

from django.contrib import admin
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.admin import UserAdmin

from profiles.models import Team, TeamMembership

User = get_user_model()


class UserCreationAdminForm(UserCreationForm):
    """
    User admin form for having the password field.
    """

    class Meta:
        model = User
        fields = "__all__"


@admin.register(User)
class UserAdminForm(UserAdmin):
    """
    User admin form with additional details.
    """

    model = User
    add_form = UserCreationAdminForm
    list_display = ["email", "username", "first_name", "last_name"]
    # Used the UserAdmin form fieldset and added the extra field requirered.
    fieldsets = (
        (None, {"fields": ("username", "password")}),
        (
            _("Personal info"),
            {"fields": ("first_name", "last_name", "email", "display_name")},
        ),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (_("Important dates"), {"fields": ("last_login", "date_joined")}),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "username",
                    "password1",
                    "password2",
                    "email",
                    "first_name",
                    "last_name",
                ),
            },
        ),
    )


class TeamMembershipInline(admin.TabularInline):
    """
    Team Membership Inline.
    """

    model = TeamMembership
    fields = ("user", "tags")
    raw_id_fields = ("user",)


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    """
    Team model admin.
    """

    inlines = [TeamMembershipInline]
    raw_id_fields = ("members",)
