"""
Permission classes relevant to workflow module.
"""
from django.contrib.auth.base_user import AbstractBaseUser

from rest_framework.permissions import BasePermission
from rest_framework.request import Request
from rest_framework.views import APIView

from workflow.protocols import AssignedModel, AuthoredModel, TeamModel


class IsAuthor(BasePermission):
    """
    Check to see if the user matches the 'author' attribute of a given object.
    """

    message = "Only the author may perform this action."

    def has_object_permission(
        self, request: Request, view: APIView, obj: AuthoredModel
    ) -> bool:
        """
        Check if the user matches the 'author' attribute of this object.
        """
        return request.user == obj.author


class IsUser(BasePermission):
    """
    Verify if the current user is the one we're looking up data about.
    """

    message = "You must be the target user to perform this action."

    def has_object_permission(
        self, request: Request, view: APIView, obj: AbstractBaseUser
    ) -> bool:
        """
        Check if the user is the right one.
        """
        return request.user == obj


class IsAssignee(BasePermission):
    """
    Verify the current user is a checklist's assignee.
    """

    message = "You are not the assignee."

    def has_object_permission(
        self, request: Request, view: APIView, obj: AssignedModel
    ) -> bool:
        """
        Check if the user is the assignee.
        """
        return request.user == obj.assignee


class IsOnTheTeam(BasePermission):
    """
    Verify the current user is on the same team of the checklist
    """

    message = "You must be on the same team to permorm this action"

    def has_object_permission(
        self, request: Request, view: APIView, obj: TeamModel
    ) -> bool:
        return obj.team in request.user.teams.all()
