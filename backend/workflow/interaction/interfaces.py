"""
Interfaces class representing interfaces and user interactions.
"""
from fractions import Fraction

from django.contrib.postgres.fields.jsonb import KeyTextTransform
from django.db import models
from django.db.models.functions import Cast
from django.utils.translation import gettext_lazy as _, ngettext

from jsonschema import validate as schema_validate
from jsonschema.exceptions import ValidationError as JSONSchemaValidationError
from utils.exceptions import raise_django_validation_error, raise_rest_validation_error

from workflow.interaction.objects import (
    Array,
    Boolean,
    NonnegativeInt,
    Number,
    UnicodeString,
)
from workflow.report.trends_builder import NumericTrendBuilder


class BaseInterface:
    """Base class for the interface objects."""

    _customization_args_fields_name = None
    _response_fields_name = None

    @classmethod
    def get_customization_args_schema(cls):
        """Returns the schema of customization_args.

        Returns:
            dict. A dict representing the schema of customization args.
        """
        schema = {}
        for field_name in cls._customization_args_fields_name:
            field_class = getattr(cls, field_name)
            schema[field_name] = field_class.get_schema()
        return {
            "type": "object",
            "properties": schema,
            "required": list(cls._customization_args_fields_name),
            "additionalProperties": False,
        }

    @classmethod
    def get_response_schema(cls):
        """Returns the schema of response.

        Returns:
            dict. A dict representing the schema of response.
        """
        schema = {}
        for field_name in cls._response_fields_name:
            field_class = getattr(cls, field_name)
            schema[field_name] = field_class.get_schema()
        return {
            "type": "object",
            "properties": schema,
            "required": list(cls._response_fields_name),
            "additionalProperties": False,
        }

    @classmethod
    def get_customization_args_default_value(cls):
        """Returns the default value for customization_args.

        Returns:
            dict. The default value for the customization args.
        """
        default_value = {}
        for field_name in cls._customization_args_fields_name:
            field_class = getattr(cls, field_name)
            default_value[field_name] = field_class.default_value
        return default_value

    @classmethod
    def validate_customization_args(
        cls, customization_args, **kwargs
    ):  # pylint: disable=R0201
        """Validates the given customization args value against its schema.

        Raises:
            ValidationError: A validation error for invalid customization args
                value.
        """
        schema = cls.get_customization_args_schema()
        try:
            schema_validate(customization_args, schema)
        except JSONSchemaValidationError as error:
            raise_django_validation_error(
                "customization_args",
                _("Invalid value for {path}: {msg}").format(
                    path=error.json_path, msg=error.message
                ),
                error=error,
            )

    @classmethod
    def validate_response_against_schema(cls, response):
        """Validates the given response value against its schema.

        Raises:
            ValidationError: A validation error for invalid response value.
        """
        schema = cls.get_response_schema()
        try:
            schema_validate(response, schema)
        except JSONSchemaValidationError as error:
            raise_rest_validation_error(
                "response",
                _("Invalid value for {path}: {msg}").format(
                    path=error.json_path, msg=error.message
                ),
                error=error,
            )

    @classmethod
    def validate_response(cls, response, _):
        """Validates the given response value.

        Raises:
            ValidationError: A validation error for invalid response value.
        """
        cls.validate_response_against_schema(response)

    @classmethod
    def can_mark_completed(cls, response, customization_args):
        """Validate whether the user has responded to the task.

        Args:
            response: dict. A dict containing user's response.
            customization_args: dict. A dictionary containing the customization args
                for this instance.

        Returns:
            bool. Whether the user has responded to the task.
        """
        raise NotImplementedError(
            "This method is expected to be implemented in the sub-class."
        )

    @classmethod
    def to_str(cls, response, customization_args):
        """Get str value for display purpose

        Args:
            response: dict. A dict containing user's response.
            customization_args: dict. A dictionary containing the customization args
                for this instance.

        Returns:
            str. String value.
        """
        raise NotImplementedError(
            "This method is expected to be implemented in the sub-class."
        )

    # pylint: disable=W0613
    @classmethod
    def accumulate_interface_instances(cls, queryset) -> dict:
        """
        Accumulate data for multiple instances of interface, empty dict as default

        Args:
            queryset: QuerySet. A queryset of completed task instances
        """
        return {}

    @classmethod
    def combine_stats_builder(cls):
        """
        create stats builder instance for combining statistics
        """
        return None


# pylint: disable=R0903
class BaseNumericInterface:
    """
    Base class for interface that has numeric value
    """

    _numeric_value_key: str = None
    _model_cast_field = None

    @classmethod
    def combine_stats_builder(cls):
        """
        create stats builder instance for combining statistics
        """
        return NumericTrendBuilder(cls._numeric_value_key)

    @classmethod
    def accumulate_interface_instances(cls, queryset) -> dict:
        """
        Override to accumulate numeric data for multiple instances

        Args:
            queryset: QuerySet. Queryset of completed instances (not null responses)
        """
        if not queryset:
            return {}
        query_key: str = cls._numeric_value_key
        cast_field = cls._model_cast_field
        acc_result: dict = {}
        annotate_dict: dict = {
            query_key: Cast(KeyTextTransform(query_key, "response"), cast_field)
        }
        parsed_queryset = queryset.annotate(**annotate_dict).filter(
            ~(models.Q(**{query_key: None}))
        )

        group_values = parsed_queryset.values(query_key, "user")
        buckets = {}
        for group in group_values:
            bucket = buckets.get(group[query_key], {})
            bucket[query_key] = group[query_key]
            bucket["users"] = bucket.get("users", [])
            bucket["users"].append(group["user"])
            buckets[group[query_key]] = bucket

        acc_result["buckets"] = list(buckets.values())
        return acc_result


class MarkdownTextAreaInterface(BaseInterface):
    """Interface for markdown textarea."""

    _customization_args_fields_name = ("placeholder",)
    _response_fields_name = ("content",)

    placeholder = UnicodeString(
        description="Placeholder for the textarea", default="Type your answer here..."
    )

    content = UnicodeString()

    @classmethod
    def can_mark_completed(cls, response, _):
        """Whether the user response can be considered completed.

        Args:
            response: dict. A dict containing user's response.

        Returns:
            bool. Whether the user response can be considered completed.
        """
        return len(response["content"]) > 0

    @classmethod
    def to_str(cls, response, customization_args) -> str:
        """Get str value for display purpose

        Args:
            response: dict. A dict containing user's response.
            customization_args: dict. A dictionary containing the customization args
                for this instance.

        Returns:
            str. String value.
        """
        try:
            return response["content"]
        except (TypeError, KeyError):
            pass
        return ""


class LinearScaleRatingInterface(BaseNumericInterface, BaseInterface):
    """Interface for linear scale rating."""

    _customization_args_fields_name = (
        "max_value",
        "min_value",
        "max_value_description",
        "min_value_description",
    )
    _response_fields_name = ("rating",)
    _numeric_value_key = "rating"
    _model_cast_field = models.IntegerField()

    max_value = NonnegativeInt(description="Maximum rating value", default=5)
    min_value = NonnegativeInt(description="Minimum rating value", default=1)
    max_value_description = UnicodeString(
        description="Max value description", default="Good/Best"
    )
    min_value_description = UnicodeString(
        description="Min value description", default="Bad/Worst"
    )

    rating = NonnegativeInt()

    @classmethod
    def validate_response(cls, response, customization_args):
        """Validates the given response value.

        Raises:
            ValidationError: A validation error for invalid response value.
        """
        cls.validate_response_against_schema(response)
        min_value = customization_args["min_value"]
        max_value = customization_args["max_value"]
        rating = response["rating"]
        if rating > max_value or rating < min_value:
            msg = _(
                "Expected user rating to be in between {min_value}"
                " and {max_value}, found {rating}"
            ).format(max_value=max_value, min_value=min_value, rating=rating)
            raise_rest_validation_error("response", msg)

    @classmethod
    def can_mark_completed(cls, response, customization_args):
        """Whether the user response can be considered completed.

        Args:
            response: dict. A dict containing user's response.

        Returns:
            bool. Whether the user response can be considered completed.
        """
        return "rating" in response

    @classmethod
    def to_str(cls, response, customization_args):
        """Get str value for display purpose

        Args:
            response: dict. A dict containing user's response.
            customization_args: dict. A dictionary containing the customization args
                for this instance.

        Returns:
            str. String value.
        """
        try:
            max_value = customization_args["max_value"]
            return f"{response['rating']} / {max_value}"
        except (TypeError, KeyError):
            pass
        return ""


class CheckboxInterface(BaseInterface):
    """Interface for checkbox."""

    _customization_args_fields_name = ()
    _response_fields_name = ("checked",)

    checked = Boolean()

    @classmethod
    def can_mark_completed(cls, response, _):
        """Whether the user response can be considered completed."""
        return response["checked"]

    @classmethod
    def to_str(cls, response, _):
        """Get str value for display purpose

        Args:
            response: dict. A dict containing user's response.
            customization_args: dict. A dictionary containing the customization args
                for this instance.

        Returns:
            str. String value.
        """
        try:
            if response["checked"]:
                return "checked"
        except (TypeError, KeyError):
            pass
        return "not checked"


class NumericInterface(BaseNumericInterface, BaseInterface):
    """Interface for numeric input."""

    _customization_args_fields_name = (
        "max_value",
        "min_value",
        "step",
    )
    _response_fields_name = ("number",)
    _numeric_value_key = "number"
    _model_cast_field = models.FloatField()

    max_value = Number(description="Maximum numeric value")
    min_value = Number(description="Minimum numeric value")
    step = Number(description="Step value")

    number = Number()

    @classmethod
    def validate_customization_args(cls, customization_args, **kwargs):
        """Validates the given customization args value against its schema.

        Raises:
            ValidationError: A validation error for invalid customization args
                value.
        """
        min_value = customization_args["min_value"]
        max_value = customization_args["max_value"]
        if min_value is not None and max_value is not None and max_value <= min_value:
            raise_django_validation_error(
                "customization_args",
                _("Maximum value is greater than or equal to minimum"),
            )
        return super().validate_customization_args(customization_args, **kwargs)

    @classmethod
    def validate_response(cls, response, customization_args):
        """Validates the given response value.

        Raises:
            ValidationError: A validation error for invalid response value.
        """
        cls.validate_response_against_schema(response)
        min_value = customization_args["min_value"]
        max_value = customization_args["max_value"]
        step = customization_args["step"]
        number = response["number"]

        if min_value is not None and number < min_value:
            raise_rest_validation_error(
                "response",
                _("Expected value to be greater than {min_value}").format(
                    min_value=min_value
                ),
            )
        if max_value is not None and number > max_value:
            raise_rest_validation_error(
                "response",
                _("Expected value to be less than {max_value}").format(
                    max_value=max_value
                ),
            )

        # https://stackoverflow.com/questions/588004/is-floating-point-math-broken
        if step and Fraction(number / step).limit_denominator(1000).denominator != 1:
            raise_rest_validation_error(
                "response",
                _("Expected value to be a multiple of {step}, found {number}").format(
                    step=step, number=number
                ),
            )

    @classmethod
    def can_mark_completed(cls, response, customization_args):
        """Whether the user response can be considered completed.

        Args:
            response: dict. A dict containing user's response.

        Returns:
            bool. Whether the user response can be considered completed.
        """
        return "number" in response

    @classmethod
    def to_str(cls, response, customization_args):
        """Get str value for display purpose

        Args:
            response: dict. A dict containing user's response.
            customization_args: dict. A dictionary containing the customization args
                for this instance.

        Returns:
            str. String value.
        """
        try:
            return str(response["number"])
        except (TypeError, KeyError):
            pass
        return ""


class MultiChoiceInterface(BaseInterface):
    """Interface for numeric input."""

    _customization_args_fields_name = (
        "minimum_selected",
        "maximum_selected",
        "available_choices",
    )
    _response_fields_name = ("selected_choices",)

    minimum_selected = NonnegativeInt(
        default=1, description="Minimum number of choices to select"
    )
    maximum_selected = Number(description="Maximum number of choices to select")

    selected_choices = Array(content_type="string", min_items=0)
    available_choices = Array(content_type="string")

    @classmethod
    def validate_response(cls, response, customization_args):
        """Validates the given response value.

        Raises:
            ValidationError: A validation error for invalid response value.
        """
        cls.validate_response_against_schema(response)
        maximum_selected = customization_args["maximum_selected"]
        selected = response["selected_choices"]
        selected_count = len(selected)

        if maximum_selected is not None and selected_count > maximum_selected:
            msg = ngettext(
                "Expected a max of {maximum_selected} choice to be selected",
                "Expected a max of {maximum_selected} choices to be selected",
                maximum_selected,
            ).format(maximum_selected=maximum_selected)
            raise_rest_validation_error("response", msg)
        if not set(selected).issubset(customization_args["available_choices"]):
            raise_rest_validation_error("response", _("Invalid choice"))

    @classmethod
    def validate_customization_args(cls, customization_args, **kwargs):
        customization_args["available_choices"] = [
            choice for choice in customization_args["available_choices"] if choice
        ]
        maximum_selected = customization_args["maximum_selected"]
        minimum_selected = customization_args["minimum_selected"]
        if maximum_selected and minimum_selected > maximum_selected:
            raise_django_validation_error(
                "customization_args",
                _("Maximum count cannot be less than minimum count!"),
            )
        if minimum_selected and minimum_selected > len(
            customization_args["available_choices"]
        ):
            raise_django_validation_error(
                "customization_args",
                _("Less number of available choices than minimum count!"),
            )
        return super().validate_customization_args(customization_args, **kwargs)

    @classmethod
    def can_mark_completed(cls, response, customization_args):
        """Whether the user response can be considered completed.

        Args:
            response: dict. A dict containing user's response.

        Returns:
            bool. Whether the user response can be considered completed.
        """
        has_response = "selected_choices" in response
        minimum_selected = customization_args["minimum_selected"]
        maximum_selected = customization_args["maximum_selected"]
        selected_count = len(response["selected_choices"])

        has_selected_min_options = (
            minimum_selected is None or selected_count >= minimum_selected
        )
        has_selected_not_more_than_max = (
            maximum_selected is None or selected_count <= maximum_selected
        )
        return (
            has_response and has_selected_min_options and has_selected_not_more_than_max
        )

    @classmethod
    def to_str(cls, response, customization_args):
        """Get str value for display purpose

        Args:
            response: dict. A dict containing user's response.
            customization_args: dict. A dictionary containing the customization args
                for this instance.

        Returns:
            str. String value.
        """
        try:
            return ", ".join(response["selected_choices"])
        except (TypeError, KeyError):
            pass
        return None


class SubsectionInterface(BaseInterface):
    """Interface for subsection."""

    _customization_args_fields_name = ()
    _response_fields_name = ()

    @classmethod
    def to_str(cls, response, customization_args):
        return None

    @classmethod
    def can_mark_completed(cls, response, customization_args):
        return True
