"""
Django admin module for workflow.
"""

from django import forms
from django.contrib import admin
from django.db import models
from django.forms import Textarea
from django.forms.models import BaseInlineFormSet, InlineForeignKeyField
from django_better_admin_arrayfield.admin.mixins import DynamicArrayMixin

from workflow.interaction import (
    get_interfaces_customization_arg_default_value,
    get_interfaces_customization_arg_schema,
)
from workflow.models import (
    Checklist,
    ChecklistDefinition,
    ChecklistTask,
    ChecklistTaskDefinition,
    Email,
    EventRecurrenceMapping,
    Recurrence,
    Run,
    RunTrendTaskReport,
)

from workflow.widgets import SchemaEditorWidget


class ChecklistTaskDefinitionForm(forms.ModelForm):
    """
    ChecklistTaskDefinition form.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["interface_type"].widget.attrs["class"] = "add-schema-editor"

    class Meta:
        model = ChecklistTaskDefinition

        fields = "__all__"


class ChecklistTaskDefinitionInlineBase(admin.TabularInline):
    """
    Base class for add/edit ChecklistTaskDefinition on the same page as parent models
    """

    form = ChecklistTaskDefinitionForm
    model = ChecklistTaskDefinition
    extra = 0
    readonly_fields = ("id_readonly",)

    # pylint: disable=no-self-use
    @admin.display(description="ID")
    def id_readonly(self, instance):
        """
        This is a workaround to display empty value for id field in admin UI
        """
        return instance.id if instance.checklist_definition else None

    exclude = (
        "created_on",
        "updated_on",
    )
    # SchemaEditorWidget renders forms based on required fields for the selected
    # interface_type.
    formfield_overrides = {
        models.JSONField: {
            "widget": SchemaEditorWidget(
                "interface_type",
                get_interfaces_customization_arg_schema(),
                get_interfaces_customization_arg_default_value(),
            )
        },
        models.TextField: {"widget": Textarea(attrs={"rows": 4, "cols": 40})},
    }
    show_change_link = True


class ChecklistTaskDefinitionInlineForChecklistFormSet(BaseInlineFormSet):
    """
    Customized FormSet to handle inline tasks creation for checklist definitions
    """

    def __init__(self, *args, **kwargs):
        kwargs["queryset"] = kwargs["queryset"].filter(parent__isnull=True)
        super().__init__(*args, **kwargs)

    def add_fields(self, form, index):
        super().add_fields(form, index)
        form.fields["parent"] = InlineForeignKeyField(None)


class ChecklistTaskDefinitionInlineForChecklist(ChecklistTaskDefinitionInlineBase):
    """
    add/edit ChecklistTaskDefinition on the same page as ChecklistDefinition
    """

    formset = ChecklistTaskDefinitionInlineForChecklistFormSet


class ChecklistTaskDefinitionInlineForTaskFormSet(BaseInlineFormSet):
    """
    Customized FormSet to handle inline tasks creation for parent task definitions
    """

    def add_fields(self, form, index):
        super().add_fields(form, index)
        form.fields["checklist_definition"] = InlineForeignKeyField(
            self.instance.checklist_definition
        )


class ChecklistTaskDefinitionInlineForTask(ChecklistTaskDefinitionInlineBase):
    """
    add/edit ChecklistTaskDefinition on the same page as parent task definition
    """

    formset = ChecklistTaskDefinitionInlineForTaskFormSet


@admin.register(ChecklistDefinition)
class ChecklistDefinitionAdmin(admin.ModelAdmin):
    """
    ChecklistDefinition model admin.
    """

    raw_id_fields = (
        "author",
        "team",
    )
    inlines = [
        ChecklistTaskDefinitionInlineForChecklist,
    ]


@admin.register(ChecklistTaskDefinition)
class ChecklistTaskDefinitionAdmin(admin.ModelAdmin):
    """
    ChecklistTaskDefinition model admin.
    """

    raw_id_fields = ("checklist_definition",)
    list_display = (
        "label",
        "checklist_definition",
        "parent",
    )
    exclude = ("parent",)

    def get_inlines(self, request, obj):
        """
        Dynamically specify inline only if there's no parent task.
        That helps enforce only one level deep of tasks nesting.
        """
        return (
            [
                ChecklistTaskDefinitionInlineForTask,
            ]
            if not obj.parent
            else []
        )


@admin.register(Email)
class EmailModelAdmin(admin.ModelAdmin, DynamicArrayMixin):
    """
    Email model admin.
    """

    list_display = ("subject", "reply_to")


@admin.register(Recurrence)
class RecurrenceAdmin(admin.ModelAdmin, DynamicArrayMixin):
    """
    Recurrence model admin.
    """

    list_display = (
        "__str__",
        "active",
        "interval_schedule",
        "start_date",
        "due_days_no",
    )
    exclude = ("periodic_task",)
    raw_id_fields = ("team",)

    def get_form(self, request, obj=None, change=False, **kwargs):
        """
        Return a Form class for use in the admin add view. This is used by
        add_view and change_view.
        """
        form = super().get_form(request, obj=None, change=False, **kwargs)

        # remove interval_schedule edit and delete option
        def _disable_edit_delete(field_name):
            field = form.base_fields[field_name]
            field.widget.can_change_related = False
            field.widget.can_delete_related = False

        _disable_edit_delete("interval_schedule")
        _disable_edit_delete("notification_email")
        _disable_edit_delete("reminder_email")
        return form


@admin.register(Run)
class RunAdmin(admin.ModelAdmin, DynamicArrayMixin):
    """
    Recurrence model admin.
    """

    list_display = (
        "__str__",
        "team",
        "start_date",
        "end_date",
        "due_date",
        "recurrence",
    )
    raw_id_fields = ("team",)


@admin.register(Checklist)
class ChecklistAdmin(admin.ModelAdmin):
    """
    Recurrence model admin.
    """

    list_display = ("__str__", "assignee", "completed", "created_on", "status")
    raw_id_fields = (
        "definition",
        "assignee",
        "run",
        "team",
    )


@admin.register(ChecklistTask)
class ChecklistTaskAdmin(admin.ModelAdmin):
    """
    ChecklistTask model admin
    """

    list_display = (
        "label",
        "assignee",
        "checklist",
        "parent",
    )


@admin.register(RunTrendTaskReport)
class RunTrendTaskReportAdmin(admin.ModelAdmin):
    """
    RunTrendTaskReport model admin.
    """

    list_display = ("task_definition", "run")


@admin.register(EventRecurrenceMapping)
class EventRecurrenceMappingAdmin(admin.ModelAdmin):
    """
    EventRecurrenceMapping model admin.
    """

    list_display = ("id", "name", "recurrence", "notes")
