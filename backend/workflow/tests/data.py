# pylint: disable=R0401
"""
Parameterized data for tests.
"""

INTERFACE_SCENARIOS = (
    # The interface tests have the following indexed arguments:
    # 1. The name of the interface being tested.
    # 2. The customization_args for the task definition
    # 3. The response being posted by the user.
    # 4. Whether the task should be considered complete (True/False). If this is None,
    #   expect a validation error.
    # Checkboxes
    ("checkbox", {}, {}, None),
    ("checkbox", {}, {"checked": True}, True),
    # Markdown containers
    ("markdown_textarea", {"placeholder": "Type stuff"}, {}, None),
    ("markdown_textarea", {"placeholder": "Type stuff"}, {"content": ""}, False),
    ("markdown_textarea", {"placeholder": "Type stuff"}, {"content": "Stuff"}, True),
    # Rating
    (
        "linear_scale_rating",
        {
            "min_value": 0,
            "max_value": 5,
            "max_value_description": "Max",
            "min_value_description": "Min",
        },
        {},
        None,
    ),
    (
        "linear_scale_rating",
        {
            "min_value": 0,
            "max_value": 5,
            "max_value_description": "Max",
            "min_value_description": "Min",
        },
        {"rating": 3},
        True,
    ),
    (
        "linear_scale_rating",
        {
            "min_value": 0,
            "max_value": 5,
            "max_value_description": "Max",
            "min_value_description": "Min",
        },
        {"rating": 7},
        None,
    ),
)
