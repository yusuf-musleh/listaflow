"""
Factories for Workflow module tests.
"""
from django.utils import timezone

from django_celery_beat.models import MINUTES, IntervalSchedule
from factory import (
    Faker,
    LazyFunction,
    SelfAttribute,
    SubFactory,
    post_generation,
    lazy_attribute,
    Iterator,
)
from factory.django import DjangoModelFactory

from profiles.tests.factories import UserFactory, UserWithNameFactory
from workflow.interaction import (
    CHECKBOX_INTERFACE,
    INTERFACE_TYPE_CHOICES,
    get_interfaces_customization_arg_default_value,
)
from workflow.models import (
    Checklist,
    ChecklistDefinition,
    ChecklistTask,
    ChecklistTaskDefinition,
    Email,
    EventRecurrenceMapping,
    Recurrence,
    Run,
)


DEFAULT_CUSTOMIZATION_ARGS = get_interfaces_customization_arg_default_value()
DEFAULT_CUSTOMIZATION_ARGS["multi_choice"]["available_choices"] = [
    "Choice A",
    "Choice B",
]


class ChecklistDefinitionFactory(DjangoModelFactory):
    """
    ChecklistDefinition factory.
    """

    class Meta:
        model = ChecklistDefinition

    name = Faker("name")
    body = Faker("paragraph")
    author = SubFactory(UserFactory)


class ChecklistTaskDefinitionFactory(DjangoModelFactory):
    """
    ChecklistTaskDefinition factory.
    """

    class Meta:
        model = ChecklistTaskDefinition

    checklist_definition = SubFactory(ChecklistDefinitionFactory)
    label = Faker("name")
    body = Faker("paragraph")
    required = Faker("pybool")

    @post_generation
    def tags(self, create, extracted, **kwargs):
        """
        Create and assign tags upon instance creation.
        """
        if not create:
            return

        if extracted:
            # pylint: disable=no-member
            self.tags.add(*extracted)


class ChecklistFactory(DjangoModelFactory):
    """
    Checklist Factory.
    """

    class Meta:
        model = Checklist

    definition = SubFactory(ChecklistDefinitionFactory)
    name = Faker("name")
    assignee = SubFactory(UserFactory)


class ChecklistTaskFactory(DjangoModelFactory):
    """
    Task factory.
    """

    class Meta:
        model = ChecklistTask

    definition = SubFactory(ChecklistTaskDefinitionFactory)
    checklist = SubFactory(
        ChecklistFactory, definition=SelfAttribute("..definition.checklist_definition")
    )


class RunFactory(DjangoModelFactory):
    """
    Run factory.
    """

    class Meta:
        model = Run

    start_date = Faker("date")
    end_date = Faker("date")


class IntervalScheduleFactory(DjangoModelFactory):
    """
    IntervalSchedule factory.
    """

    class Meta:
        model = IntervalSchedule
        django_get_or_create = ["every", "period"]

    every = 1
    period = MINUTES


class RecurrenceFactory(DjangoModelFactory):
    """
    Recurrence factory.
    """

    class Meta:
        model = Recurrence
        django_get_or_create = ["team", "interval_schedule", "checklist_definition"]

    start_date = LazyFunction(timezone.now)
    interval_schedule = SubFactory(IntervalScheduleFactory)

    @post_generation
    def tags(self, create, extracted, **kwargs):
        """
        Create and assign tags upon instance creation.
        """
        if not create:
            return

        if extracted:
            # pylint: disable=no-member
            self.tags.add(*extracted)


class EmailFactory(DjangoModelFactory):
    """
    email templates factory
    """

    class Meta:
        model = Email

    subject = Faker("name")
    text_body = Faker("paragraph")
    html_body = Faker("paragraph")


class EventRecurrenceMappingFactory(DjangoModelFactory):
    """
    EventRecurrenceMapping factory.
    """

    class Meta:
        model = EventRecurrenceMapping

    name = Faker("name")
    recurrence = SubFactory(RecurrenceFactory)
    notes = Faker("paragraph")


class ChecklistDefinitionWithColorFactory(DjangoModelFactory):
    """Create a Checklist Definition object with color name"""

    class Meta:
        model = ChecklistDefinition
        django_get_or_create = ("name",)

    name = Faker("color_name")
    author = SubFactory(UserWithNameFactory)
    can_create_one_off = True


class ChecklistTaskDefinitionWithColorFactory(DjangoModelFactory):
    """Create a Checklist Task Definition object with color name"""

    class Meta:
        model = ChecklistTaskDefinition
        django_get_or_create = ("label",)

    # pylint: disable=missing-class-docstring,too-few-public-methods
    class Params:
        label_color = Faker("color_name")

    checklist_definition = SubFactory(ChecklistDefinitionWithColorFactory)
    body = Faker("paragraph")
    required = Faker("pybool")
    interface_type = Iterator(INTERFACE_TYPE_CHOICES, getter=lambda i: i[0])

    @lazy_attribute
    def label(self):
        """Task label"""
        return f"{self.label_color} {self.interface_type}"

    @lazy_attribute
    def customization_args(self):
        """Customization args"""
        return DEFAULT_CUSTOMIZATION_ARGS[self.interface_type]


class ChecklistDefinitionWithAllTaskTypes(ChecklistDefinitionWithColorFactory):
    """Create a Checklist Definition with all available task types"""

    @post_generation
    def checklist(self, create, _, **__):
        """Create batch of checklist task definitions"""
        if not create:
            return

        ChecklistTaskDefinitionWithColorFactory.create_batch(
            size=len(INTERFACE_TYPE_CHOICES), checklist_definition=self
        )
        subsection = self.task_definitions.get(interface_type="subsection")
        ChecklistTaskDefinitionWithColorFactory.create_batch(
            size=2,
            checklist_definition=self,
            parent=subsection,
            interface_type=CHECKBOX_INTERFACE,
        )


class RecurringChecklist(ChecklistDefinitionWithAllTaskTypes):
    """Create a recurring checklist definition that is not active"""

    @post_generation
    def recurrence(self, create, _, **__):
        """Create a recurrence with interval"""
        if not create:
            return

        RecurrenceFactory(
            active=False,
            interval_schedule=IntervalScheduleFactory(every=5),
            checklist_definition=self,
            team=self.team,
        )
