"""
Useful snippets/abstract models for the project.
"""
from django.db import models

# Create your models here.
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from taggit.models import TagBase, ItemBase


class AutoDateTimeField(models.DateTimeField):
    """
    Field for automatically setting the time on a field when it's updated.
    This is preferable to auto_now, which has several subtle bugs involving
    timezones.
    """

    def pre_save(self, model_instance, add):
        return timezone.now()


class DateTrackedModel(models.Model):
    """
    Used on models that will need to be tracked on creation and update.
    """

    created_on = models.DateTimeField(db_index=True, default=timezone.now)
    updated_on = AutoDateTimeField(db_index=True, default=timezone.now)

    class Meta:
        abstract = True


class Tag(TagBase):
    """
    Custom tag class. As yet, we have no need for customization on the tag class
    itself, however, we're electing to use custom tag through tables, and if we're
    going that far, we may as well make a custom tag model to make any future needed
    changes not require massive migrations if we need it later.
    """

    class Meta:
        verbose_name = _("Tag")
        verbose_name_plural = _("Tags")


class TaggedItemBase(ItemBase):
    """
    Same here as with Tag-- basically copy-pasted from upstream per django-taggit's
    docs.
    """

    tag = models.ForeignKey(
        Tag, related_name="%(app_label)s_%(class)s_items", on_delete=models.CASCADE
    )

    class Meta:
        abstract = True
