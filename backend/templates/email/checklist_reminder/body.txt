Hey, {{ display_name }}! ⏰

Time's almost up to complete {{ checklist_name }} for {{ run_start_month_name }} {{ run_start_day_of_month }} to {{ run_end_month_name }} {{ run_end_day_of_month }}. Please add your update before the end of day today ({{ due_date_day_of_week }}, {{ due_date_month_name }} {{ due_date_day_of_month }}).

Your team is awaiting your response on the following items:
{% for item in task_list %}
* {{ item }}
{% endfor %}
Link: {{ site_url }}/lists/{{ checklist_id }}
