# Internationalization

## Backend - Translation

1. Mark strings for translation with [Django gettext functions](https://docs.djangoproject.com/en/3.2/topics/i18n/translation/).
    > Note: To give hints to translators, add comments with `Translators:` prefix, further details in [translation docs](https://docs.djangoproject.com/en/3.2/topics/i18n/translation/#comments-for-translators).

2. Update messages files for all languages with the new translatable strings by running:

    ```sh
    make makemessages.backend
    ```

    > Note: If adding a new language `messages.backend` will not create the initial translation file so will need to:

    ```sh
    make shell
    python manage.py makemessages -l <new_language_code>
    ```

3. Add translations for new strings in messages files located in `backend/locale/`.
4. To generate binary `*.mo` files from `*.po` messages files run:
    ```sh
    make compilemessages.backend
    ```
5. Commit changed `*.po` and `*.mo` files.


## Frontend - Translation

Workflow for developers when adding new text to the frontend.

> Note: The primary language, secondary languages and locales folder are specified in [locales.sync.config.js](../frontend/locales.sync.config.js).

1. To enable text for translation use supported methods from [`react-i18next` ](https://react.i18next.com/) to refer to text using a key.
2. Add text's key and text to primary language locale file.
    > e.g. For English (en) open `en/translation.json` in the locales folder.
3. Sync translation files for the secondary languages by running:
    ```sh
    make makemessages.frontend
    ```
4. Update translations in secondary languages locale files, where possible.
5. Commit all changed JSON translation files
6. Verify translations in browser by appending query string with language code to URL e.g. for Arabic set `lng` to `ar`:

    ```
    localhost:8000/?lng=ar
    ```

    By default the language is cached in localStorage under the key `i18nextLng`. You can delete the key via browser console to reset to your system language:

    ```
    delete localStorage.i18nextLng
    ```

    Afterwards, refresh the page for the changes to take effect.


## Frontend - Working with RTL interface

- When embedding user-generated content that possibly has mixed left-to-right (LTR) and right-to-left (RTL) content, make sure to use [`bdi`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/bdi) tag to wrap it. This HTML tag is also helpful in case you want to keep direction of a text put in a mixed content (e.g: date string).
- Add `dir=auto` attribute to text input elements to allow input of bi-directional content.
- We use [react-markdown](https://www.npmjs.com/package/react-markdown) for displaying and inputting markdown content. If it's a must to add wrapper to rendered elements, override the component in [ReactMarkdownOverrides.tsx](./src/components/ReactMarkdownOverrides.tsx) and make sure these overrides passed to ReactMarkdown using [components](https://github.com/remarkjs/react-markdown#appendix-b-components) attribute.

    ```jsx
    import ReactMarkdownOverrides from "./ReactMarkdownOverrides";
    ...
      return (
        <ReactMarkdown
          children={text}
          remarkPlugins={[remarkGfm]}
          components={ReactMarkdownOverrides}
        />
      );
    ...
    ```
