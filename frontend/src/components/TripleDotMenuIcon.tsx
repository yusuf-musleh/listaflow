export const TripleDotMenuIcon = () => {
  return (
    <div className="triple-dot-menu-icon">
      <div className="dots"></div>
    </div>
  );
};
