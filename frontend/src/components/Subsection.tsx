import { SingleController } from "@opencraft/providence/base/singles/types/SingleController";
import { ReactElement } from "react";
import Accordion from "react-bootstrap/Accordion";
import { useTranslation } from "react-i18next";

import { Task } from "../types/Task";
import { Title } from "./Title";
import Badge from "react-bootstrap/Badge";

declare interface SubsectionItem {
  controller: SingleController<Task>;
  isCompleted: boolean;
  children: ReactElement[];
}

export const Subsection = ({
  controller,
  isCompleted,
  children,
}: SubsectionItem) => {
  const { t } = useTranslation();

  return (
    <Accordion.Item
      className="border-top border-light-navy border-2 checklist-subsection"
      eventKey={controller.x!.id}
      key={controller.x!.id}
    >
      <Accordion.Button
        tabIndex={undefined}
        aria-label={t("taskItem.showDetails")}
        className=""
      >
        <div className="w-100 d-flex justify-content-between">
          <div dir="auto">
            {isCompleted ? (
              <del>
                <Title text={controller.x!.label} />
              </del>
            ) : (
              <Title text={controller.x!.label} />
            )}
            &nbsp;
            <Badge pill bg="secondary">
              {controller.x!.children.length}
            </Badge>
          </div>
        </div>
      </Accordion.Button>
      {children && <Accordion.Body>{children}</Accordion.Body>}
    </Accordion.Item>
  );
};
