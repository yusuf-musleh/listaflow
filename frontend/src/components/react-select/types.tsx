import { Options } from "react-select";

export type ValueCountFunc = (value: Options<any>) => number;
