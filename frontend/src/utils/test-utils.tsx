import { FC, ReactElement } from "react";
import { render, RenderOptions } from "@testing-library/react";
import {
  defaultContextValues,
  ProvidenceContext,
} from "@opencraft/providence/react-plugin/context";
import { Provider } from "react-redux";
import { createStore, IModuleStore } from "redux-dynamic-modules";
import { fieldLength, requiredField } from "./validators";
import { BrowserRouter } from "react-router-dom";

const AllTheProviders: FC = ({ children }) => {
  const store: IModuleStore<{}> = createStore({});
  const buildContext = defaultContextValues();
  buildContext.validators.required = requiredField;
  buildContext.validators.length = fieldLength;

  return (
    <Provider store={store}>
      <ProvidenceContext.Provider value={buildContext}>
        <BrowserRouter>{children}</BrowserRouter>
      </ProvidenceContext.Provider>
    </Provider>
  );
};

const customRender = (
  ui: ReactElement,
  options?: Omit<RenderOptions, "wrapper">
) => render(ui, { wrapper: AllTheProviders, ...options });

export * from "@testing-library/react";
export { customRender as render };

const scheduler =
  typeof setImmediate === "function" ? setImmediate : setTimeout;

export const flushPromises = () => {
  return new Promise(function (resolve) {
    scheduler(resolve);
  });
};
