module.exports = {
  primaryLanguage: "en",
  secondaryLanguages: ["ar", "vi"],
  localesFolder: "./public/static/locales",
};
